This repository contains daemons which manage various settings in a Bananui session.

This is similar to what `gnome-settings-daemon` does in a GNOME session.

## List of daemons

- `bananui-settings-modem`
   - manages SIM slot naming
- `bananui-settings-power`
   - manages keyboard and display backlight
