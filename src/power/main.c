/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <errno.h>
#include <fcntl.h>
#include <glob.h>
#include <unistd.h>
#include "backlight.h"
#include "switch.h"

typedef struct _BPowerObjects
{
  BPowerBacklight *disp_bl;
  BPowerBacklight *kbd_bl;
  BPowerSwitch *flip_sw;
  GMainLoop *loop;
  GPtrArray *event_sources;
} BPowerObjects;

static void
on_bus_acquired (GDBusConnection *connection,
                 const char      *name,
                 gpointer         user_data)
{
  BPowerObjects *objects = user_data;

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON(objects->flip_sw),
                                    connection,
                                    "/de/abscue/obp/Bananui/Switch/flip",
                                    NULL);

  if (objects->kbd_bl) {
    g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON(objects->kbd_bl),
                                      connection,
                                      "/de/abscue/obp/Bananui/Backlight/keyboard",
                                      NULL);
  }

  if (objects->disp_bl) {
    g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON(objects->disp_bl),
                                      connection,
                                      "/de/abscue/obp/Bananui/Backlight/display",
                                      NULL);
  }
}

static void
on_name_acquired (GDBusConnection *connection,
                  const char      *name,
                  gpointer         user_data)
{
  g_info ("Acquired name %s", name);
}

static void
on_name_lost (GDBusConnection *connection,
              const char      *name,
              gpointer         user_data)
{
  BPowerObjects *objects = user_data;

  g_info ("Lost name %s", name);
  g_main_loop_quit (objects->loop);
}

static gboolean
on_input_event (GIOChannel   *source,
                GIOCondition  condition,
                gpointer      user_data)
{
  BPowerObjects *objects = user_data;
  struct input_event ev;
  int fd = g_io_channel_unix_get_fd (source);

  if (read (fd, &ev, sizeof(ev)) != sizeof(ev)) {
    g_warning ("Failed to read from input: %s",
               errno ? strerror(errno) : "No data returned");
    return TRUE;
  }

  b_power_switch_handle_event (objects->flip_sw, &ev);

  return TRUE;
}

int main(void)
{
  int i;
  glob_t gb;
  GIOChannel *ch;
  BPowerObjects objects = { 0 };

  objects.flip_sw = b_power_switch_new (SW_LID, EV_SW);
  objects.disp_bl = b_power_backlight_find ("/sys/class/backlight/*");
  if (!objects.disp_bl)
    g_warning ("No display backlight found");
  objects.kbd_bl = b_power_backlight_find ("/sys/class/leds/*:kbd_backlight");
  if (!objects.kbd_bl)
    g_warning ("No keyboard backlight found");

  glob("/dev/input/event*", GLOB_NOSORT, NULL, &gb);
  for (i = 0; i < gb.gl_pathc; i++) {
    int fd = open(gb.gl_pathv[i], O_RDONLY);

    if (fd < 0) {
      g_warning ("Failed to open %s: %s", gb.gl_pathv[i], strerror (errno));
      continue;
    }

    ch = g_io_channel_unix_new (fd);
    g_io_add_watch (ch, G_IO_IN, on_input_event, &objects);
    g_io_channel_unref (ch);
  }
  globfree(&gb);

  g_bus_own_name (G_BUS_TYPE_SESSION,
                  "de.abscue.obp.Bananui.Power",
                  G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                  G_BUS_NAME_OWNER_FLAGS_REPLACE,
                  on_bus_acquired,
                  on_name_acquired,
                  on_name_lost,
                  &objects,
                  NULL);

  objects.loop = g_main_loop_new(NULL, TRUE);
  g_main_loop_run(objects.loop);

  return 0;
}
