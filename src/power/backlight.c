/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <glob.h>
#include "backlight.h"

enum {
  PROP_0,
  PROP_PATH,
  PROP_LAST_PROP
};
static GParamSpec *props[PROP_LAST_PROP];

static void backlight_interface_init (DBusBacklightIface *iface);

typedef struct _BPowerBacklight
{
  DBusBacklightSkeleton  parent;
  char                  *brightness_path;
  unsigned int           brightness;
  unsigned int           max_brightness;
} BPowerBacklight;

G_DEFINE_TYPE_WITH_CODE (BPowerBacklight,
                         b_power_backlight,
                         DBUS_TYPE_BACKLIGHT_SKELETON,
                         G_IMPLEMENT_INTERFACE (
                           DBUS_TYPE_BACKLIGHT,
                           backlight_interface_init));


static void
update_brightness (BPowerBacklight *self)
{
  unsigned int percentage = 100;

  if (self->max_brightness > 0) {
    percentage *= self->brightness;
    percentage /= self->max_brightness;
  }

  if (percentage != dbus_backlight_get_brightness (DBUS_BACKLIGHT (self)))
    dbus_backlight_set_brightness (DBUS_BACKLIGHT (self), percentage);
}


static void
set_brightness (BPowerBacklight *self, unsigned int brightness)
{
  if (self->brightness_path) {
    FILE *f = fopen (self->brightness_path, "w");

    if (f) {
      fprintf (f, "%u\n", brightness);
      fclose (f);
    } else {
      g_warning ("Failed to open file '%s' for writing", self->brightness_path);
    }
  }

  if (brightness > self->max_brightness)
    self->brightness = self->max_brightness;
  else
    self->brightness = brightness;
  update_brightness (self);
}


static gboolean
backlight_handle_decrease (DBusBacklight         *dbus_backlight,
                           GDBusMethodInvocation *invocation)
{
  BPowerBacklight *self = B_POWER_BACKLIGHT (dbus_backlight);

  if (self->brightness > 0) {
    set_brightness (self, self->brightness - 1);
  }

  dbus_backlight_complete_decrease (dbus_backlight, invocation);

  return TRUE;
}


static gboolean
backlight_handle_increase (DBusBacklight         *dbus_backlight,
                           GDBusMethodInvocation *invocation)
{
  BPowerBacklight *self = B_POWER_BACKLIGHT (dbus_backlight);

  if (self->brightness < self->max_brightness) {
    set_brightness (self, self->brightness + 1);
  }

  dbus_backlight_complete_increase (dbus_backlight, invocation);

  return TRUE;
}


static void
brightness_changed_cb (BPowerBacklight *self,
                       GParamSpec      *pspec,
                       gpointer         user_data)
{
  unsigned int brightness;

  brightness = dbus_backlight_get_brightness (DBUS_BACKLIGHT (self));
  brightness *= self->max_brightness;
  if (brightness % 100)
    brightness = brightness / 100 + 1;
  else
    brightness /= 100;

  set_brightness (self, brightness);
}


static void
backlight_interface_init (DBusBacklightIface *iface)
{
  iface->handle_decrease = backlight_handle_decrease;
  iface->handle_increase = backlight_handle_increase;
}


static void
backlight_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  BPowerBacklight *self = B_POWER_BACKLIGHT (object);
  char *max_brightness_path;
  FILE *f;

  switch (property_id) {
  case PROP_PATH:
    self->brightness_path = g_strdup_printf ("%s/brightness",
                                             g_value_get_string (value));
    max_brightness_path = g_strdup_printf ("%s/max_brightness",
                                           g_value_get_string (value));
    f = fopen (max_brightness_path, "r");
    if (!f) {
      g_warning ("Failed to open file '%s'", max_brightness_path);
      g_free (max_brightness_path);
      self->max_brightness = 0;
      break;
    }
    if (fscanf (f, "%u", &self->max_brightness) != 1) {
      g_warning ("Failed to read brightness from '%s'", max_brightness_path);
      self->max_brightness = 0;
    }
    fclose (f);
    g_free (max_brightness_path);
    f = fopen (self->brightness_path, "r");
    if (!f) {
      g_warning ("Failed to open file '%s'", self->brightness_path);
      self->brightness = 0;
      break;
    }
    if (fscanf (f, "%u", &self->brightness) != 1) {
      g_warning ("Failed to read brightness from '%s'", self->brightness_path);
      self->brightness = 0;
    }
    fclose (f);
    update_brightness (self);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
backlight_constructed (GObject *object)
{
  BPowerBacklight *self = B_POWER_BACKLIGHT (object);

  g_signal_connect (self, "notify::brightness",
                    G_CALLBACK (brightness_changed_cb), NULL);

  G_OBJECT_CLASS (b_power_backlight_parent_class)->constructed (object);
}

static void
b_power_backlight_class_init (BPowerBacklightClass *self_class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (self_class);

  object_class->constructed = backlight_constructed;
  object_class->set_property = backlight_set_property;

  props[PROP_PATH] =
    g_param_spec_string ("path",
                         "Path",
                         "path to the sysfs backlight interface",
                         NULL,
                         G_PARAM_WRITABLE |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


static void
b_power_backlight_init (BPowerBacklight *self)
{
}


BPowerBacklight *
b_power_backlight_find (const char *glob_path)
{
  glob_t gb;
  BPowerBacklight *bl;

  glob (glob_path, 0, NULL, &gb);
  if (gb.gl_pathc == 0)
    bl = NULL;
  else
    bl = g_object_new (B_POWER_TYPE_BACKLIGHT, "path", gb.gl_pathv[0], NULL);
  globfree (&gb);

  return bl;
}

