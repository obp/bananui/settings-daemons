/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SWITCH_H_
#define _SWITCH_H_

#include "../dbus/switch-dbus.h"

#include <linux/input.h>

#define B_POWER_TYPE_SWITCH (b_power_switch_get_type ())

G_DECLARE_FINAL_TYPE(BPowerSwitch,
		     b_power_switch,
		     B_POWER,
		     SWITCH,
		     DBusSwitchSkeleton);

BPowerSwitch *b_power_switch_new          (int keycode, int event);
void          b_power_switch_handle_event (BPowerSwitch *self,
                                           struct input_event *ev);

#endif
