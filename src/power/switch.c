/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <glob.h>
#include "switch.h"

enum {
  PROP_0,
  PROP_KEYCODE,
  PROP_EVENT,
  PROP_LAST_PROP
};
static GParamSpec *props[PROP_LAST_PROP];

static void switch_interface_init (DBusSwitchIface *iface);

typedef struct _BPowerSwitch
{
  DBusSwitchSkeleton     parent;
  int                    keycode;
  int                    event;
} BPowerSwitch;

G_DEFINE_TYPE_WITH_CODE (BPowerSwitch,
                         b_power_switch,
                         DBUS_TYPE_SWITCH_SKELETON,
                         G_IMPLEMENT_INTERFACE (
                           DBUS_TYPE_SWITCH,
                           switch_interface_init));


static void
switch_interface_init (DBusSwitchIface *iface)
{
}


static void
switch_set_property (GObject      *object,
                     guint         property_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  BPowerSwitch *self = B_POWER_SWITCH (object);

  switch (property_id) {
  case PROP_KEYCODE:
    self->keycode = g_value_get_int (value);
    break;
  case PROP_EVENT:
    self->event = g_value_get_int (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
switch_constructed (GObject *object)
{
  G_OBJECT_CLASS (b_power_switch_parent_class)->constructed (object);
}


static void
b_power_switch_class_init (BPowerSwitchClass *self_class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (self_class);

  object_class->constructed = switch_constructed;
  object_class->set_property = switch_set_property;

  props[PROP_KEYCODE] =
    g_param_spec_int ("keycode",
                      "Key code",
                      "Linux key code emitted by the system for this switch",
                      0, INT_MAX,
                      0,
                      G_PARAM_WRITABLE |
                      G_PARAM_STATIC_STRINGS);

  props[PROP_EVENT] =
    g_param_spec_int ("event",
                      "Event type",
                      "Type of input event emitted by the system for this switch",
                      0, EV_MAX,
                      0,
                      G_PARAM_WRITABLE |
                      G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


static void
b_power_switch_init (BPowerSwitch *self)
{
}


void
b_power_switch_handle_event (BPowerSwitch *self, struct input_event *ev)
{
  if (self->event != ev->type || self->keycode != ev->code)
    return;

  if (ev->value)
    g_object_set (self, "state", "closed", NULL);
  else
    g_object_set (self, "state", "open", NULL);
}


BPowerSwitch *
b_power_switch_new (int keycode, int event)
{
  return g_object_new (B_POWER_TYPE_SWITCH,
                       "state", "unknown",
                       "keycode", keycode,
                       "event", event,
                       NULL);
}

