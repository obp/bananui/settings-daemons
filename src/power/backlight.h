/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BACKLIGHT_H_
#define _BACKLIGHT_H_

#include "../dbus/backlight-dbus.h"

#define B_POWER_TYPE_BACKLIGHT (b_power_backlight_get_type ())

G_DECLARE_FINAL_TYPE(BPowerBacklight,
		     b_power_backlight,
		     B_POWER,
		     BACKLIGHT,
		     DBusBacklightSkeleton);

BPowerBacklight *b_power_backlight_find (const char *glob_path);

#endif
