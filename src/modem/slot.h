/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SLOT_H_
#define _SLOT_H_

#include "../dbus/sim-slot-dbus.h"
#include "../dbus/wroomd-dbus.h"

#define B_MODEM_TYPE_SLOT (b_modem_slot_get_type ())

G_DECLARE_FINAL_TYPE(BModemSlot,
		     b_modem_slot,
		     B_MODEM,
		     SLOT,
		     DBusSIMSlotSkeleton);

BModemSlot *b_modem_slot_from_wroomd_slot (GKeyFile *config, WroomdDBusWroomdSlot *slot);

#endif
