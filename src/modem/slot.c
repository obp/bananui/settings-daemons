/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "slot.h"

enum {
  PROP_0,
  PROP_CONFIG,
  PROP_WROOMD_SLOT,
  PROP_LAST_PROP
};
static GParamSpec *props[PROP_LAST_PROP];

enum {
  SIGNAL_CHANGED,
  N_SIGNALS,
};
static unsigned int signals[N_SIGNALS];

static void slot_interface_init (DBusSIMSlotIface *iface);

typedef struct _BModemSlot
{
  DBusSIMSlotSkeleton   parent;
  GKeyFile             *config;
  WroomdDBusWroomdSlot *slot;
} BModemSlot;

G_DEFINE_TYPE_WITH_CODE (BModemSlot,
                         b_modem_slot,
                         DBUS_TYPE_SIMSLOT_SKELETON,
                         G_IMPLEMENT_INTERFACE (
                           DBUS_TYPE_SIMSLOT,
                           slot_interface_init));


static void
slot_interface_init (DBusSIMSlotIface *iface)
{
}


static void
b_modem_slot_init (BModemSlot *self)
{
}


static void
priority_changed_cb (BModemSlot           *self,
                     WroomdDBusWroomdSlot *slot,
                     GParamSpec           *pspec)
{
  unsigned int priority = wroomd_dbus_wroomd_slot_get_priority (slot);

  g_key_file_set_integer (
    self->config,
    dbus_simslot_get_wroomd_path (DBUS_SIMSLOT(self)),
    "Priority",
    priority);

  g_signal_emit (self, signals[SIGNAL_CHANGED], 0);
}


static void
slot_init_config (BModemSlot *self)
{
  const char *name;
  
  name = g_key_file_get_string (
    self->config,
    dbus_simslot_get_wroomd_path (DBUS_SIMSLOT(self)),
    "Name",
    NULL);
  dbus_simslot_set_name (DBUS_SIMSLOT(self), name ? : "SIM");

  priority_changed_cb (self, self->slot, NULL);

  g_signal_connect_object (self->slot,
                           "notify::priority",
                           G_CALLBACK(priority_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

static void
slot_set_property (GObject      *object,
                   guint         property_id,
                   const GValue *value,
                   GParamSpec   *pspec)
{
  BModemSlot *self = B_MODEM_SLOT (object);

  switch (property_id) {
  case PROP_CONFIG:
    self->config = g_value_dup_boxed (value);
    if (self->slot)
      slot_init_config (self);
    break;
  case PROP_WROOMD_SLOT:
    self->slot = g_value_dup_object (value);
    dbus_simslot_set_wroomd_path (DBUS_SIMSLOT(self),
                                  g_dbus_object_get_object_path (
                                    g_dbus_interface_get_object (
                                      G_DBUS_INTERFACE(self->slot))));
    if (self->config)
      slot_init_config (self);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
name_changed_cb (BModemSlot *self,
                 GParamSpec *pspec,
                 gpointer    user_data)
{
  g_autoptr(GError)  err  = NULL;
  const char        *name = dbus_simslot_get_name (DBUS_SIMSLOT (self));

  g_key_file_set_string (self->config,
                         dbus_simslot_get_wroomd_path (DBUS_SIMSLOT(self)),
                         "Name",
                         name);

  g_signal_emit (self, signals[SIGNAL_CHANGED], 0);
}

static void
slot_constructed (GObject *object)
{
  BModemSlot *self = B_MODEM_SLOT (object);

  G_OBJECT_CLASS (b_modem_slot_parent_class)->constructed (object);

  g_signal_connect (self, "notify::name",
		    G_CALLBACK (name_changed_cb), NULL);
}


static void
b_modem_slot_class_init (BModemSlotClass *self_class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (self_class);

  object_class->constructed = slot_constructed;
  object_class->set_property = slot_set_property;

  props[PROP_CONFIG] =
    g_param_spec_boxed ("config", NULL, NULL,
                        G_TYPE_KEY_FILE,
                        G_PARAM_WRITABLE);
  props[PROP_WROOMD_SLOT] =
    g_param_spec_object ("wroomd-slot", NULL, NULL,
                         WROOMD_DBUS_TYPE_WROOMD_SLOT,
                         G_PARAM_WRITABLE);

  signals[SIGNAL_CHANGED] =
    g_signal_new ("changed",
                  B_MODEM_TYPE_SLOT,
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  0);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


BModemSlot *
b_modem_slot_from_wroomd_slot (GKeyFile *config, WroomdDBusWroomdSlot *slot)
{
  return g_object_new (B_MODEM_TYPE_SLOT,
                       "config", config,
                       "wroomd-slot", slot,
                       NULL);
}
