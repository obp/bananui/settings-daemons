/*
 * This file is part of bananui-settings.
 * Copyright (C) 2023 The OpenBananaProject contributors
 *
 * bananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "slot.h"

typedef struct _BModemObjects
{
  GKeyFile                 *config;
  char                     *config_path;
  GMainLoop                *loop;
  GHashTable               *slots;
  GDBusObjectManager       *manager;
  GDBusObjectManagerServer *server;
  GDBusConnection          *connection;
  int                       unique_number;
} BModemObjects;


static void
config_changed_cb (BModemSlot    *slot,
                   BModemObjects *objects)
{
  g_autoptr(GError) err = NULL;

  g_debug ("Saving configuration");

  if (!g_key_file_save_to_file (objects->config, objects->config_path, &err))
    g_warning ("Failed to save configuration: %s", err->message);
}


static void
interface_added_cb (BModemObjects      *objects,
                    GDBusObject        *object,
                    GDBusInterface     *iface)
{
  const char *path = g_dbus_object_get_object_path (object);
  if (WROOMD_DBUS_IS_WROOMD_SLOT(iface)) {
    GDBusObjectSkeleton *obj;
    BModemSlot *slot;
    char *path2;
    g_debug ("Found slot at %s", path);
    if (g_hash_table_lookup (objects->slots, path)) {
      g_debug ("Slot already handled, ignoring.");
      return;
    }
    slot = b_modem_slot_from_wroomd_slot (objects->config,
                                          WROOMD_DBUS_WROOMD_SLOT (iface));
    g_signal_connect (slot, "changed",
                      G_CALLBACK(config_changed_cb), objects);
    g_hash_table_insert (objects->slots, g_strdup (path), slot);
    path2 = g_strdup_printf ("/de/abscue/obp/Bananui/SIMSlot/%d",
                             objects->unique_number++);
    obj = g_dbus_object_skeleton_new (path2);
    g_dbus_object_skeleton_add_interface (obj, G_DBUS_INTERFACE_SKELETON(slot));
    g_dbus_object_manager_server_export (objects->server, obj);
    g_object_unref (obj);
    g_free (path2);
  } else if (WROOMD_DBUS_IS_WROOMD_CALL(iface)) {
    g_debug ("Found call at %s", path);
  }
}


static void
interface_removed_cb (BModemObjects      *objects,
                      GDBusObject        *object,
                      GDBusInterface     *iface)
{
  const char *path = g_dbus_object_get_object_path (object);

  if (WROOMD_DBUS_IS_WROOMD_SLOT(iface)) {
    gpointer slot, path2;

    g_hash_table_steal_extended (objects->slots, path, &path2, &slot);
    g_dbus_object_manager_server_unexport
      (objects->server,
       g_dbus_object_get_object_path (
         g_dbus_interface_get_object (
           G_DBUS_INTERFACE(slot))));
    g_free (path2);
    g_object_unref (slot);
    g_debug ("Removed slot at %s", path);
  } else if (WROOMD_DBUS_IS_WROOMD_CALL(iface)) {
    g_debug ("Removed call at %s", path);
  }
}


static void
object_added_cb (BModemObjects      *objects,
                 GDBusObject        *object)
{
  GList *ifaces, *iface;

  ifaces = g_dbus_object_get_interfaces (object);
  for (iface = ifaces; iface; iface = iface->next) {
    interface_added_cb (objects, object, G_DBUS_INTERFACE(iface->data));
  }
}


static void
object_removed_cb (BModemObjects      *objects,
                   GDBusObject        *object)
{
  GList *ifaces, *iface;

  ifaces = g_dbus_object_get_interfaces (object);
  for (iface = ifaces; iface; iface = iface->next) {
    interface_removed_cb (objects, object, G_DBUS_INTERFACE(iface->data));
  }
}


static void
add_objects (BModemObjects *objects)
{
  GList *all, *obj;

  all = g_dbus_object_manager_get_objects (objects->manager);
  for (obj = all; obj; obj = obj->next) {
    object_added_cb (objects, G_DBUS_OBJECT (obj->data));
  }

  g_list_free_full (all, g_object_unref);
}


static void
object_manager_cb (GDBusConnection *connection,
                   GAsyncResult    *res,
                   BModemObjects   *objects)
{
  g_autoptr(GError) error = NULL;

  objects->manager = wroomd_dbus_object_manager_client_new_for_bus_finish (res, &error);
  if (!objects->manager) {
    g_warning ("Error creating object manager client for wroomd: %s", error->message);
    return;
  }

  g_signal_connect_swapped (objects->manager,
                            "interface-added",
                            G_CALLBACK (interface_added_cb), objects);
  g_signal_connect_swapped (objects->manager,
                            "interface-removed",
                            G_CALLBACK (interface_removed_cb), objects);
  g_signal_connect_swapped (objects->manager,
                            "object-added",
                            G_CALLBACK (object_added_cb), objects);
  g_signal_connect_swapped (objects->manager,
                            "object-removed",
                            G_CALLBACK (object_removed_cb), objects);
  add_objects (objects);
}

static void
on_bus_acquired (GDBusConnection *connection,
                 const char      *name,
                 gpointer         user_data)
{
  BModemObjects *objects = user_data;

  objects->connection = connection;

  g_dbus_object_manager_server_set_connection (objects->server, connection);

  wroomd_dbus_object_manager_client_new_for_bus (
    G_BUS_TYPE_SYSTEM,
    G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_NONE,
    "de.abscue.obp.Wroomd",
    "/de/abscue/obp/Wroomd",
    NULL,
    (GAsyncReadyCallback) object_manager_cb,
    objects);
}

static void
on_name_acquired (GDBusConnection *connection,
                  const char      *name,
                  gpointer         user_data)
{
  g_info ("Acquired name %s", name);
}

static void
on_name_lost (GDBusConnection *connection,
              const char      *name,
              gpointer         user_data)
{
  BModemObjects *objects = user_data;

  g_info ("Lost name %s", name);
  g_main_loop_quit (objects->loop);
}

int main(void)
{
  BModemObjects  objects    = { 0 };
  char          *config_dir = g_strdup_printf ("%s/bananui", g_get_user_config_dir ());

  g_mkdir_with_parents (config_dir, 0644);
  objects.config_path = g_strdup_printf ("%s/sim-slots.ini", config_dir);
  g_free (config_dir);

  objects.config = g_key_file_new ();
  if (!g_key_file_load_from_file (objects.config,
                                  objects.config_path,
                                  G_KEY_FILE_NONE,
                                  NULL)) {
    g_autoptr(GError) err = NULL;

    g_debug ("No user config found, checking system default config");

    if (!g_key_file_load_from_file (objects.config,
                                    "/etc/bananui/sim-slots.ini",
                                    G_KEY_FILE_NONE,
                                    NULL)) {
      g_debug ("No default config found");
    }
  }

  objects.slots = g_hash_table_new_full (g_str_hash,
                                         g_str_equal,
                                         g_free,
                                         g_object_unref);

  objects.server = g_dbus_object_manager_server_new ("/de/abscue/obp/Bananui/SIMSlot");

  g_bus_own_name (G_BUS_TYPE_SESSION,
                  "de.abscue.obp.Bananui.Modem",
                  G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                  G_BUS_NAME_OWNER_FLAGS_REPLACE,
                  on_bus_acquired,
                  on_name_acquired,
                  on_name_lost,
                  &objects,
                  NULL);

  objects.loop = g_main_loop_new(NULL, TRUE);
  g_main_loop_run(objects.loop);

  return 0;
}
